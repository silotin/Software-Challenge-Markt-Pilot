 
# Vergleichsprogramm für Wolle 
## von Simon Lüdke

### Idee
Ich verwende eine abstrakte basis Klasse für das Webscraping um einfach weitere Subklassen, 
die jeweils auf eine Webseite angepasst sind hinzufügen zu können. 
Alle Daten werden in eine json Datei geschrieben, da diese sehr einfach wieder einlesbar ist. 
Wenn ein Vergleich über Zeit gewollt ist, kann man hier einfach eine weitere dictionary Ebene einfügen, die das Abfragedatum beinhaltet.
Die daten werden ausserdem als csv ausgegeben um besser von Menschen gelesen werden zu können. 
Solange keine Vergleichsfunktionalität programmiert ist, ist dies mit Sicherheit wichtiger z.B. für die strickende Oma. 




### Test
Als einfacher Systemtest wurde das Programm gestartet und die Daten, die herausgeschrieben wurden mit denen auf der Webseite abgeglichen. 
Es stellte sich heraus, dass einige Produkte (unter dem gegebenen Namen) nicht im Shop verfügbar waren. 


### Zukünfitige Funktionalität
Neben weiteren Webseiten, mit denen Preise verglichen werden können, sollte auch ein Vergleich mit den Preisen der letzten
Monate implementiert werden. Da Wolle ein haltbares Produkt ist, bietet sich ein Vorratskauf während einer günstigen Prieslage an. 
