import json


class fileWriter:
    @staticmethod
    def write_csv(product_dic : dict):
        print("Writing data to csv.")
        with open("data.csv", "w") as csv_file:
            data = "Webseite,Name,Marke,Preis,Nadelstärke,Zusammensetzung,Verfügbarkeit\n"
            for website in product_dic.keys():
                for product_name in product_dic[website].keys():
                    data += f"{website}, {product_name}"
                    try:
                        this_product = product_dic[website][product_name]
                        data += f", {this_product['brand']}, {this_product['price'].replace(',','.')}, {this_product['thickness']}, "
                        data += f"{this_product['composition']}, {this_product['availability']}\n"
                    except KeyError as key:
                        print (f"key error : {key}")
                        data += "\n"
            csv_file.write(data)

    @staticmethod
    def write_json(product_dic : dict):
        print("Writing data to json.")
        with open("data.json", "w") as json_file:
            json.dump(product_dic, json_file)
