from abstactWebScrapingBase import abstractWebScrapingBase
from bs4 import BeautifulSoup
import requests


# class specifically for scraping the website "www.wollplatz.de"
class wollPlatz(abstractWebScrapingBase):

    def __init__(self):
        self.link = "https://www.wollplatz.de/wolle"
        self.data = None
        self.product_page = None
        self.product_dic = {}


    def get_relevant_products(self, product_list):
        product_link_list = []
        for product in product_list:
            link = self.link + "/" + product[0].lower() + "-" + product[1].replace(" ", "-").lower()
            product_link_list.append(link)
        return product_link_list

    def get_product_information(self, product, link):
        name = product[1]
        brand = product[0]

        source = requests.get(link).text
        product_page = BeautifulSoup(source, 'lxml')

        # get price
        price = product_page.find("span", class_="product-price-amount").text
        price += product_page.find("span", class_="product-price-currency").text

        # get shipping information
        shipping = product_page.find("tr", class_="pbuy-voorraad").span.text

        # iterate through info boxes to find the remaining information
        boxes = product_page.find_all("tr")
        thickness = None
        composition = None
        for b in boxes:
            # get information on the thickness
            if b.td.text == "Nadelstärke":
                thickness = b.find_all("td")[1].text
            # get information on the composition
            elif b.td.text == "Zusammenstellung":
                composition = b.find_all("td")[1].text
        self.product_dic[name] = {"brand": brand, "price": price, "thickness": thickness, "composition": composition,
                                  "availability": shipping}
