from abc import ABC, abstractmethod


# abstract base class for all website specific web scraping classes
class abstractWebScrapingBase(ABC):

    def __init__(self):
        self.product_dic = None

    @abstractmethod
    def get_relevant_products(self, product_list) -> list:
        pass

    @abstractmethod
    def get_product_information(self, product, link):
        pass

    def run(self, product_list):
        product_link_list = self.get_relevant_products(product_list)
        for i in range(len(product_list)):
            try:
                self.get_product_information(product_list[i], product_link_list[i])
            except Exception as e:
                print(f"The following exception occrred for {product_list[i][0]} - {product_list[i][1]} at the link {product_link_list[i]}")
                print(f"{e}")
                print(f"The product is probably not available on this website.")
                self.product_dic[product_list[i][1]] = {}
        return self.product_dic
