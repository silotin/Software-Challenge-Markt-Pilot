from wollplatz import wollPlatz
from filewriter import fileWriter

# list of products
products = [["DMC", "Natura XL"], ["Drops", "Safran"], ["Drops", "Baby Merino Mix"], ["Hahn", "Alpacca Speciale"],
            ["Stylecraft", "Special double knit"]]
# list ob websites to search on
websites = ["wollplatz"]
# dictionary to save the data in
info_dict = {}


def run():
    for website in websites:
        if website == "wollplatz":
            w1 = wollPlatz()
            info_dict[website] = w1.run(products)

    fileWriter.write_json(info_dict)
    fileWriter.write_csv(info_dict)


run()
